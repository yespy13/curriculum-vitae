CONSTRUYE EL CURRICULUM CON EL QUE QUIERES FINALIZAR LA FORMACIÓN
=================================================================

Tomando como punto de partida este C.V. que te dejamos, construye el curriculum ideal que te gustaría que fuera cierto para el final del programa.

* Puedes hacerlo en inglés o castellano
* No te preocupes (al principio) por el estilo. Basta con respetar el del ejemplo. Ya trabajaremos eso más adelante.

OBJETIVOS
---------

* Familiarizarse con la estructura de archivos HTML.
* Traducir un texto del inglés.
* Reflexionar sobre mis competencias actuales que son interesantes para el mercado laboral.
* Reflexionar sobre las competencias que quiero adquirir en la formación.

REQUISITOS
----------

* Cuenta en http://repl.it, si no la tienes, ahora es el momento de abrirte una.

